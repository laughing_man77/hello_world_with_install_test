Hello World Processor
=====================

ApiOpenStudio is a modular and pluggable framework, that allows you to add
custom and contributed [processors][wiki_processors] to extend the
functionality.

This is a very simple example "Hello world!" plugin processor for
[ApiOpenStudio][api_open_studio]. It returns "Hello world!" text.

Getting started
---------------

### Including this in a Composer project

In your `composer.json` file, add a `repositories` section:

    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/laughing_man77/hello_world"
        }
    ],

In the `require` or `require-dev` section, add:

    "laughing_man77/hello_world": "dev-master"

To update composer and add the repository, run:

    $ composer update

### Using the new processor

ApiOpenStudio automatically includes and calls processors as it runs through
the resource metadata, using the `peocessor` key/pair to identify the processor
by its machine-name.

By default, ApiOpenStudio assumes that processors used in resources are in the
`\ApiOpenStudio\Processor\` namespace, and so do not need a namespace prefix. To
let ApiOpenStudio know that it needs to fetch the processor from a different
namespace, the machine-name needs to include the namespace. For example

Default processor (core) example:

    processor: user_delete

3rd party processor (example_processor) example:

    processor: \Laughing_man77\HelloWorld

### Example resource metadata

    name: Hello world
    description: Hello world
    uri: hello_world
    method: get
    appid: 2
    ttl: 0
    process:
        processor: \Laughing_man77\HelloWorld
        id: hello world

Forking the repository
----------------------

To fork the repository, to use it as a starting point for your project:

* Visit the [hello_world][hello_world] repository
* Click on the `Fork` button at the top of the page.
* Enter your `Project name` and any other settings that you need.

Links
-----

* [ApiOpenStudio][api_open_studio]
* [Processors documentation][wiki_processors]
* [Repository][hello_world]

[api_open_studio]: https://www.apiopenstudio.com
[wiki_processors]: https://wiki.apiopenstudio.com/developers/processors.html
[hello_world]: https://gitlab.com/laughing_man77/hello_world